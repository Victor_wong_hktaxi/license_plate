from userimageski import UserData

def main(name):
    # creates instance of class and loads image    
    user = UserData(name)
    # plots preprocessed imae 
    user.plot_preprocessed_image()
    # detects objects in preprocessed image
    candidates = user.get_text_candidates()
    # plots objects detected
    digits = user.plot_to_check(candidates, 'Total Objects Detected')

    return digits
