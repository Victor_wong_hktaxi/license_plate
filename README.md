# License Plate Detection #
------------------------------------------------------------------------------------
## Program flow ##
1 Crop the boundaries of the frame

2 Find the plate

   - Convert the image into gray color

   - Increase the color contract by equalizeHist()

   - Reduce the noise by bilateralFilter()

   - Convert the canny-image

   - Find the contour with at least length 4

   - Use convex Hull to remove the irregular polygon

   - Check the "rectangleness" and length


3 Crop out the potential plate

4 Filter the image and convert it into red and blue color

   - restoration denoise

5 Filter the image and convert it into black and white color

6 Split the text and combine it again for reducing the noise among the digits

7 Combined the digits again and put it back with a larger background

8 Recognize the text by using `pytesseract`

-----------------------------------------------------------------------

## Program to run ##
* crop_plate_class.py

------------------------------------------------------------------------
## Finish writing but not merging to the main yet ##

`* perspective_transform.py`

If the image is captured at a steep angle, we can use perspective transformation to calibrate the image

![cav5aiO.png](https://bitbucket.org/repo/ea8dpa/images/3106126488-cav5aiO.png)