import Image
import webbrowser

def main(name):
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # input the image name string
    # convert the image into black and white color

    # Return img ---- Image object instance
    # Return new_name_path ------- the new file name path
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    path_name = "./plate/" + name
    img = Image.open(path_name)
    pixels = img.load()
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            if pixels[i, j][2] < 100:
                pixels[i, j] = (0,0,0)
            else:
                pixels[i, j] = (255,255,255)
    new_name ="bw_" + name 
    new_name_path = "./plate/bw/" + new_name
    img.save(new_name_path)
    return new_name_path
