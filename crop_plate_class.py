from __future__ import division
import cv2
import sys
import Image
import os
import time
import matplotlib.pyplot as plt
import convert_bw
import linecache
import split_digit
import recognize_text
import numpy as np
import pytesseract
import scipy.misc
import denoise_plate_bw
from skimage.io import imread
from skimage.filters import threshold_otsu
from skimage.morphology import closing, square
from skimage import restoration
import perspective_transform as tf

def main():
    video_capture = cv2.VideoCapture("C:\Users\user\Desktop\car_camera/MOVI0695.mp4")
#    video_capture = cv2.VideoCapture("C:\Users\user\Desktop\car_camera/06171826_7437.mp4")
#    video_capture = cv2.VideoCapture("C:\Users\user\Desktop\car_camera/LOCK1229_7277.mp4")

    try:
        for i in os.listdir("./source"):
            if i.endswith(".jpg") or i.endswith(".jpeg"):
                while True:
                    # print "name = ", i
                    # find_license(cv2.imread("a2.jpg"))

                    ret, frame = video_capture.read()
                    frame = frame[frame.shape[0]*0.5:frame.shape[0]*0.8, frame.shape[1]*0.4:frame.shape[1]*0.7]
                    
                    dst = np.array((
                        (0, 0),
                        (0, 150),
                        (300, 150),
                        (300, 0)
                    ))
                    # source target position
                    src = np.array((
                        (265, 70),
                        (260, 186),
                        (435, 193),
                        (430, 90)
                    ))
                    plt.imshow(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
                    plt.plot(src[:, 0], src[:, 1], '.r')
#                    plt.show()
                   
#                    tf.transform(frame)
                   
                    plate =  license_plate(frame)
                    # Find the plate corrdinates in a frame
                    plate_candidates  = plate.find_license()

                    for plate_arr in plate_candidates:
                        # Crop and save the plate
                        plate.crop_plate(plate_arr)

                        # Filter and convert the plate into blue-red 
                        name = plate.filter_image()

                        ## Convert the filtered image into black-white
                        bw_name = convert_bw.main(name)

                        # Split the digits in the plate
                        digits = []
                        digits = split_digit.main(bw_name)
                        if (digits) :
                            #Combine the splitted digits into one
                            name = combine_images(digits)
                            # Enlarge the image
                            name = enlarged_centralize_image(name)
                            # Reocognize the text in the image
                            recognize_text(name)
                       
                        """"""""""""""""""""""""""""""""""""""""""""""""""""""
                       # Another method to denoise and convert the plate into black and white color 
                        """"""""""""""""""""""""""""""""""""""""""""""""""""""
                        bw_name = denoise_plate_bw.segmen(plate.cropped)
                        
                        # Split the digits in the plate
                        digits = []
                        digits = split_digit.main(bw_name)
                        if (digits) :
                            #Combine the splitted digits into one
                            name = combine_images(digits)
                            # Enlarge the image
                            name = enlarged_centralize_image(name)
                            # Reocognize the text in the image
                            recognize_text(name)
                        
                    k = cv2.waitKey(1) & 0xFF
                    if k == 27:
                        break
    except:
        PrintException()


class license_plate():

    def __init__(self, image):
        self.image = image


    def find_license(self):
        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        # Convert the image to Canny
        # Find a polygon with length 4 (rectangle) with specified length

        # Return the plate coordinates list
        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        show = self.image.copy() 

        gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)
        gray = cv2.bilateralFilter(gray, 11, 17, 17)
        edged = cv2.Canny(gray, 200, 255)

        edged = (255-edged)
        edged = cv2.Laplacian(edged, cv2.CV_8U)

        (cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cnts=sorted(cnts, key = cv2.contourArea, reverse = True)[:10]
    
        # loop over our contours
        plate_candidates = []
        for c in cnts:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)

            if len(approx) > 3:
                hull = cv2.convexHull(approx,returnPoints = True)
                hull_area = cv2.contourArea(hull) 
                if hull_area > 800 and hull_area < 3000 and self._rectangleness(hull):
                    print "hull_area = ",hull_area
                    plate_candidates.append(hull)
                    cv2.drawContours(show, [hull], -1, (0,255,0), 3)
                    
    
        cv2.imshow("", show)
        cv2.imshow("edged", edged)
        return plate_candidates

    def _rectangleness(self, hull):
        rect = cv2.boundingRect(hull)
        rectPoints = np.array([[rect[0], rect[1]], 
                               [rect[0] + rect[2], rect[1]],
                               [rect[0] + rect[2], rect[1] + rect[3]],
                               [rect[0], rect[1] + rect[3]]])
        intersection_area = cv2.intersectConvexConvex(np.array(rectPoints), hull)[0] 

        rect_area = cv2.contourArea(rectPoints)
        if rect_area > 0:
            rectangleness = intersection_area/rect_area
            if rectangleness > 0.8 and self._length_checking(rect):
                return True


    def _length_checking(self, arr):
        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        # Checking whether the length of contour is
        # within the range, return 1 if true
    
        # arr : array of contour
        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        x1 = arr[0]
        y1 = arr[1]
        x2 = arr[0] + arr[2]
        y2 = arr[1]
        x3 = arr[0] + arr[2]
        y3 = arr[1] + arr[3]
        x4 = arr[0]
        y4 = arr[1] + arr[3]
#        x1 = arr[0][0][0]
#        y1 = arr[0][0][1]
#        x2 = arr[1][0][0]
#        y2 = arr[1][0][1]
#        x3 = arr[2][0][0]
#        y3 = arr[2][0][1]
#        x4 = arr[3][0][0]
#        y4 = arr[3][0][1]
#    
        h = max(max(abs(y1-y2), abs(y1-y3)), abs(y1-y4))
        w = max(max(abs(x1-x2), abs(x1-x3)), abs(x1-x4))
    
        if h >25 or w > 73:
            return 0
        else:
            print "h = ", h
            print "w = ", w
            return 1
    

    
    name = ''
    def crop_plate (self, arr):
        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        # crop and save the plate
    
        # arr : contour array (plate array)
        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        global name
        x1 = arr[0][0][0]
        y1 = arr[0][0][1]
        x2 = arr[1][0][0]
        y2 = arr[1][0][1]
        x3 = arr[2][0][0]
        y3 = arr[2][0][1]
        x4 = arr[3][0][0]
        y4 = arr[3][0][1]
    
        h = max(max(abs(y1-y2), abs(y1-y3)), abs(y1-y4))
        w = max(max(abs(x1-x2), abs(x1-x3)), abs(x1-x4))
        
        top_most = min(min(y1, y2), min(y3, y4))
        left_most =  min(min(x1, x2), min(x3, x4))
    
    
        self.cropped = self.image[top_most:top_most+h, left_most:left_most+w]
        self.cropped = cv2.resize(self.cropped, (500, 200))
        # cv2.imshow("", cropped)
        # cv2.waitKey(0)
    
        name = "./plate/" + str(time.time()) + '.jpg'
        cv2.imwrite(name, self.cropped)
    

    def filter_image(self):
        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        # Filter the image into blue and red color
        # and save it
        
        # Return the name of the saved file
        """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        self.cropped =cv2.cvtColor(self.cropped, cv2.COLOR_BGR2GRAY)
        self.cropped = restoration.denoise_tv_chambolle(self.cropped, weight=0.1)
        thresh = threshold_otsu(self.cropped)
        bw = closing(self.cropped > thresh, square(2))
        cleared = bw.copy()
    
        plt.axis("off")
        plt.imshow(cleared)
        time_name  = str(time.time()) + '.jpg'
        name = "./plate/" + time_name 
        plt.imsave(name, cleared)
        return time_name
        
    
    def text_recognize(file_name):
        print "plate_file_name = ", file_name
        img = Image.open(file_name) 
        img = img.convert('L')
    
    
        threshold = 140  
        table = []  
        for i in range(256):  
            if i < threshold:  
                table.append(0)  
            else:  
                table.append(1)  
        img = img.point(table, '1')
    
        img.save("./plate/hi.jpg", "JPEG")
        print image_to_string(img) 




def combine_images(digits):
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # Combine the digits image into one license-plate image

    # Input: digits --- list contains arrays
    
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # Remove the blank digit 
    for digit in digits:
        if (digit==0).all() or (digit==1).all():
            digits.remove(digit)

    # Combine the digit into one
    new_img = digits[0]
    for x in range(len(digits)):
        if x == 0:     # The first digit already put into new_img, so we can ignore it
            continue
        new_img = np.concatenate((new_img, digits[x]), axis=1)


    scipy.misc.imsave("test.jpg", new_img)
    cv2.imshow("plate", new_img)
#    cv2.waitKey(0)

    return "test.jpg"
        
def enlarged_centralize_image(img):
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # Resize the image to have a larger background

    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    img = cv2.imread(img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    big = np.zeros((450, 850))

    new_img = big.copy()

    b_middle_x = int(big.shape[0]/2)
    b_middle_y = int(big.shape[1]/2)
    
    s_middle_x = int(img.shape[0]/2)
    s_middle_y = int(img.shape[1]/2)

    # Denoise
    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            if img[x][y] < 100:
                img[x][y] = 0
            else:
                 img[x][y] =255 

    # Map the original image to the large background
    for i, x in enumerate(range(b_middle_x - s_middle_x, b_middle_x + s_middle_x)):
        for j, y in enumerate(range(b_middle_y - s_middle_y, b_middle_y + s_middle_y)):
            new_img[x,y] = img[i, j] 

    cv2.imwrite("temp.jpg", new_img)
    return "temp.jpg"
    

def recognize_text(name):
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # Using pytesseract to recognize the digits in the image
    # Check if the letter within 'A'-'Z' or '0'-'9'
    # If not, remove it

    # Finally printout the correct plate number
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    number = pytesseract.image_to_string(Image.open(name))
    
    real = ""
    for letter in number:
        if 'A' <= letter <= 'Z' or '0' <= letter <= '9': 
            real +=letter
    print '-------'
    print real
    print '-------'
    print '============================================================'



def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print ('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

if __name__ == "__main__":
    main()
