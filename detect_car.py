#! /usr/bin/python
 
import cv2
 
face_cascade = cv2.CascadeClassifier('cars3.xml')
vc = cv2.VideoCapture('06171731_7414.mp4')
 
if vc.isOpened():
    rval , frame = vc.read()
else:
    rval = False
 
while rval:
    rval, frame = vc.read()
    frame = frame[frame.shape[0]*0.3:frame.shape[0]*1]
    # car detection.
    cars = face_cascade.detectMultiScale(frame, 1.1, 2)
 
    ncars = 0
    for (x,y,w,h) in cars:
        if w >90:
            cv2.rectangle(frame,(x,y),(x+w,y+h),(0,0,255),2)
        ncars = ncars + 1
 
    # show result
    cv2.imshow("Result",frame)
    cv2.waitKey(1);
vc.release()