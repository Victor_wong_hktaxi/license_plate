from PIL import Image
import pytesseract as pt
import os
import time
import cv2
import numpy as np

def main():
    for i in os.listdir("./plate"):
        if i.endswith("jpg"):
            name = "./plate/" + i
            print "name = ", name
            text_recognize(name)

def text_recognize(name):
    img = Image.open(name)
    img.load()
    img = img.convert('L')

    threshold = 140
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)
    img = img.point(table, '1')
   
    new_name = "./plate/text_recog/" + str(time.time()) + '.jpg'
    img.save(new_name, "JPEG")
    print ("{}: {}".format(name, pt.image_to_string(img)))
    print pt.image_to_string(img)

def svm(img):
    from sklearn import datasets, svm, metrics
    import matplotlib.pyplot as plt
    from scipy import misc
    from sklearn.externals import joblib
#    digits = datasets.load_digits()

#    images_and_labels = list(zip(digits.images, digits.target))
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    #for index, (image, label) in enumerate(images_and_labels[:4]):
    #    plt.subplot(2, 4, index + 1)
    #    plt.axis('off')
    #    plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
    #    plt.title('Training: %i' % label)
    
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
#    n_samples = len(digits.images)
#    data = digits.images.reshape((n_samples, -1))
#
#    classifier = svm.SVC(gamma = 0.001)
#
#    classifier.fit(data[:n_samples], digits.target[:n_samples])
    
    # joblib.dump(classifier, 'svm.pkl')
    classifier = joblib.load('svm.pkl')

#    expected = digits.target[n_samples / 2:]
#    predicted = classifier.predict(data[n_samples / 2:])
#    images_and_predictions = list(zip(digits.images[n_samples / 2:], predicted))
#    for index, (image, prediction) in enumerate(images_and_predictions[:4]):
#        plt.subplot(2, 4, index + 5)
#        plt.axis('off')
#        plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
#        plt.title('Prediction: %i' % prediction)
#    predicted = classifier.predict(data[n_samples /2:])
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

   # print "\n#########################################################"
   # print "training data info"
   # print "number = ", digits.target[i]
   # print "size = ", digits.images[i].shape
   # print "type = ", digits.images[i].dtype
   # print "data type = ", type(digits.images[i])
   # print digits.images[i]
   # print "########################################################\n"



    im = img
    # Enlarge the image
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    big = np.zeros((55,85))
    img = big.copy()
    
    b_middle_x = big.shape[0]/2
    b_middle_y = big.shape[1]/2
    
    s_middle_x = im.shape[0]/2
    s_middle_y = im.shape[1]/2

    for i, x in enumerate(range(b_middle_x - s_middle_x, b_middle_x + s_middle_x)):
        for j, y in enumerate(range(b_middle_y - s_middle_y, b_middle_y + s_middle_y)):
            img[x,y] = im[i,j] 
    

    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            if img[x][y] < 3:
                img[x][y] = 0
            else:
#                    img[x][y] = img[x][y]*16/255
                 img[x][y] = 16
    img = cv2.resize(img, (8, 8))
#        img = img.astype(digits.images[0].dtype)
    img = img.astype(int)
    img = img.reshape((1,img.shape[0]*img.shape[1]))
    predicted = classifier.predict(img)
    return predicted[0]

    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
#if __name__ == "__main__":
#    svm()
#    exit()
#    main()
