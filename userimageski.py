import numpy as np
from skimage.io import imread
from skimage.filters import threshold_otsu
from skimage.transform import resize
import cPickle
#from matplotlib import pyplot as plt
from skimage.morphology import closing, square
from skimage.measure import regionprops
from skimage import restoration
from skimage import measure
from skimage.color import label2rgb
#import matplotlib.patches as mpatches
import scipy.misc
import operator
import time
import linecache
import sys
import cv2

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print ('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

class UserData():
    """
    class in charge of dealing with User Image input.
    """
    
    def __init__(self, image_file):
        """
        reads the image provided by the user as grey scale and preprocesses it.
        """
        self.image = imread(image_file, as_grey=True)
        self.preprocess_image()
    
#############################################################################################################

    def preprocess_image(self):
        """
        Denoises and increases contrast. 
        """
        image = restoration.denoise_tv_chambolle(self.image, weight=0.1)
        thresh = threshold_otsu(image)
        self.bw = closing(image > thresh, square(2))
        self.cleared = self.bw.copy()
        return self.cleared 
    
############################################################################################################

    def plot_preprocessed_image(self):
        """
        plots pre-processed image. The plotted image is the same as obtained at the end
        of the get_text_candidates method.
        """
        image = restoration.denoise_tv_chambolle(self.image, weight=0.1)
        thresh = threshold_otsu(image)
        bw = closing(image > thresh, square(2))
        cleared = bw.copy()
        
        label_image = measure.label(cleared)
        borders = np.logical_xor(bw, cleared)
       
        label_image[borders] = -1
        image_label_overlay = label2rgb(label_image, image=image)
        
##########################################################################################################################
    def get_text_candidates(self):
        """
        identifies objects in the image.
        Gets contours, draws rectangles around them
        and saves the rectangles as individual images.
        """
        label_image = measure.label(self.cleared)   
        borders = np.logical_xor(self.bw, self.cleared)
        label_image[borders] = -1
        
        
        coordinates = []
        i=0

        samples = self.image
        for region in regionprops(label_image):
            if region.area < 300:
                continue
            else:
                minr, minc, maxr, maxc = region.bbox
                margin = 3
                minr, minc, maxr, maxc = minr-margin, minc-margin, maxr+margin, maxc+margin
                roi = self.image[minr:maxr, minc:maxc]
                if roi.shape[0]*roi.shape[1] == 0:
                    continue
                else:
                    if i==0:
                        samples = resize(roi, (50,50))
                        coordinates.append(region.bbox)
                        i+=1
                    elif i==1:
                        roismall = resize(roi, (50,50))
                        samples = np.concatenate((samples[None,:,:], roismall[None,:,:]), axis=0)
                        coordinates.append(region.bbox)
                        i+=1
                    else:
                        roismall = resize(roi, (50,50))
                        samples = np.concatenate((samples[:,:,:], roismall[None,:,:]), axis=0)
                        coordinates.append(region.bbox)
        
        self.candidates = {
                    'fullscale': samples,          
                    'flattened': samples.reshape((samples.shape[0], -1)),
                    'coordinates': np.array(coordinates)
                    }
        print 'Images After Contour Detection'
        print 'Fullscale: ', self.candidates['fullscale'].shape
        print 'Flattened: ', self.candidates['flattened'].shape
        print 'Contour Coordinates: ', self.candidates['coordinates'].shape
        return self.candidates 
    
        
############################################################################################################################
    
    def plot_to_check(self, what_to_plot, title):
        """
        Save the digits according to its position
        Left to Right

        Return the digits image array 
        """
        try:
            if len(what_to_plot['fullscale'].shape) == 3: 
                digits = []
                for name, i in enumerate(sorted(zip(what_to_plot['fullscale'], what_to_plot['coordinates']), key=lambda x: x[1][1])):
                    digits.append(i[0])
                    
                return digits
        except:
            PrintException()
