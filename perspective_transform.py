import numpy as np
import cv2
from skimage import transform as tf
from skimage import img_as_ubyte


def transform(frame):
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # Transform the warp images (left hand side of the frame)

    # Return and transformed image
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # destination image size
    dst = np.array((
        (0, 0),
        (0, 150),
        (300, 150),
        (300, 0)
    ))
    # source target position
    src = np.array((
        (265, 70),
        (260, 186),
        (435, 193),
        (430, 90)
    ))
    tform3 = tf.ProjectiveTransform()
    tform3.estimate(dst, src)
    warped = tf.warp(frame, tform3, output_shape = (150, 300))

    # Convert skimage to cv2
    warped_cv2 = img_as_ubyte(warped)
    cv2.imshow("transform", warped_cv2)

    return warped_cv2
